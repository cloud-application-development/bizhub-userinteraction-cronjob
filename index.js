console.log("Starting job execution...")

const serviceAccount = require('./service_account_keyfile.json');
const Firestore = require("@google-cloud/firestore");

const ROOT_COLLECTION = 'tenants';
const POSTS_COLLECTION = 'posts';
const COMMENTS_COLLECTION = 'comments';
const LIKES_COLLECTION = 'likes';

const METADATA_COLLECTION = 'metadata';
const POST_INTERACTION_DOCUMENT = 'post_interactions';

const db = new Firestore({
    projectId: serviceAccount.project_id,
    keyFilename: './service_account_keyfile.json',
});



let commentCount = 0;
let likeCount = 0;

const calculatePostInteractionCountsButCooler = async () => {
    const tenantsRef = db.collection(ROOT_COLLECTION)
    const tenants = await tenantsRef.get()
    for (const tenant of tenants.docs) {
        const postCollectionRef = tenantsRef.doc(tenant.id).collection(POSTS_COLLECTION)
        const posts = await postCollectionRef.get()
        for (const postDocument of posts.docs) {
            const commentCollectionRef = postCollectionRef.doc(postDocument.id).collection(COMMENTS_COLLECTION)
            const likeCollectionRef = postCollectionRef.doc(postDocument.id).collection(LIKES_COLLECTION)

            const commentsQuerySnapshot = await commentCollectionRef.get()
            const likesQuerySnapshot = await likeCollectionRef.get()

            commentCount += commentsQuerySnapshot.size;
            likeCount += likesQuerySnapshot.size;
        }
    }


}

const updatePostInteractionCountsInFirestore = async () => {
    db.collection(METADATA_COLLECTION).doc(POST_INTERACTION_DOCUMENT).set({
        commentCount: commentCount,
        likeCount: likeCount,
    })
}


console.log("starting count")
let performPostInteractionCount = async () => {
    await calculatePostInteractionCountsButCooler()
    console.log("commentCount: "+ commentCount)
    console.log("likeCount: "+ likeCount)
    await updatePostInteractionCountsInFirestore()
    console.log("Job ended.")
}

performPostInteractionCount();

