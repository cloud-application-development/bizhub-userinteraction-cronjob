FROM node:17-alpine3.12
WORKDIR /app
ADD package*.json ./
RUN npm install
ADD index.js ./
RUN apk update && apk add bash